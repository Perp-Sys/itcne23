# Einführung
## Installation Multipass
 [Link](https://multipass.run/install)

## Basic Multipass Commands
|Command   	                                    |Description                          | 
|---	                                        |---	                              |
|multipass launch                               |#Create VM                           |
|multipass exec -n "instance" "command"         |#execute command on vm               |
|multipass shell "instance" 	                |#connect via ssh to the vm           |
|multipass networks     	                    |list available network configurations|
|multipass ls           	                    |#list available vm's                 |
|multipass start "name" 	                    |#start vm                            |
|multipass find	                                |#list available images               |
|multipass delete "instance"	                |#delete vm                           |
|multipass purge           	                    |#purge vm                            |
|multipass transfer "file" "instance/path"      |#transfer file to vm                 |
|multipass launch "--network network-name"      |#create a vm with a choosen network  |
|multipass launch -n "name" --cloud-init cloud-config.yaml| #Create VM with Cloud-Init-File|

## Aufgabe: Erstellen einer VM mit einer einfachen Cloud-Init Konfiguration

Erstellt eine VM mit multipass, in welcher ihr folgende Dinge mit Cloud-Init konfiguriert:

- [Gruppen und User](https://cloudinit.readthedocs.io/en/latest/reference/modules.html#users-and-groups):
  - erzeugt eine Gruppe `students`.
  - erzeugt die User bob und john die `students` als [Primary Group](https://docs.oracle.com/cd/E19253-01/817-1985/userconcept-35906/index.html) haben und als [Secondary Group](https://docs.oracle.com/cd/E19253-01/817-1985/userconcept-35906/index.html) `sudo`
  - Setzt Passwörter für die beiden user mit [chpasswd](https://cloudinit.readthedocs.io/en/latest/reference/modules.html#set-passwords) und erlaubt ssh-login.

## Cloud-Init-File
````YAML 
#cloud-config
fqdn: perptest
ssh_pwauth: true
groups:
  - students
  - sudo
users:
  - default
  - name: bob
    lock_passwd: false
    primary_group: students
    groups: sudo
  - name: john
    lock_passwd: false
    primary_group: students
    groups: sudo
chpasswd:
  expire: false
  users:
  - name: bob
    password: Abc1234
    type: text
  - name: john
    password: Abc1234
    type: text
````
## Auftrag: Logfiles

- Findet im Logfile heraus, woher cloud-init euer cloud-init script bekommt,
- und wo es dieses hinschreibt (cached).   

Tip: sucht nach `user-data`

### Antwort
- /var/log/cloud-init.log: Hier kommt der Debugging output von den Cloud-Init-Skripten rein. Das ist wo wir die wichtigsten Infos finden
- /var/log/cloud-init-output.log: Hier kommt der output von Kommands rein. Meistens nicht so interessant

## Auftrag: Cache-Directory

Untersucht den Inhalt des Cache-Verzeichnisses. Welche Inhalte findet ihr dort

### Antwort
/var/lib/cloud/data

| Dateninhalte        |                      |                |              |
|---------------------|----------------------|----------------|--------------|
| instance-id         | previous-hostname    | python-version | set-hostname |
| previous-datasource | previous-instance-id | result.json    | status.json  |

## Auftrag: Stages

- Versucht im Logfile, die [hier](https://cloudinit.readthedocs.io/en/latest/explanation/boot.html) beschreibenen Bootstages (Local, Network, Config, Final), zu finden, 
- und versucht herauszufinden was in welchen Stages konfiguriert wird. 

### Antwort
[Cloud Init Boot Stages](https://cloudinit.readthedocs.io/en/latest/explanation/boot.html)

## Auftrag: Datasource

Im Logfile solltet ihr eigentlich gefunden haben woher die user-data geholt wurde.
Nur ein paar Zeilen davor seht ihr, welches Device als Datasource gefunden und gemounted wird.

Tip: sucht nach der ersten Zeile im Logfile welches `DataSource` enthält

- Mounted das Iso-Image und schaut euch den Inhalt an. Was findet ihr dort? 

Datasourcen sind ziemlich wichtig, darum sollte man verstehen was darin übermittelt wird.

### Antwort
Unter umständen wird ein ISO image gemounted.