#1
• Erstelle eine neue Ressourcengruppe "myResourceGroup" 
		○ Region: nach eurer Wahl
• Innerhalb dieser Ressourcengruppe erstellst du eine AppService "myWebApp"
	
![alt text](https://gitlab.com/hf-itcne23/portfolio/-/raw/main/04_ITCNE23_Azure/Resource%20Gruppe%20erstellen/webapp.png)	
	
• Die Web-App wird auf einem App Service Plan mit dem Namen "myAppServicePlan" ausgeführt.
	
• Die Lösung soll so günstig wie möglich sein. Tipp: SKU beachten

![alt text](https://gitlab.com/hf-itcne23/portfolio/-/raw/main/04_ITCNE23_Azure/Resource%20Gruppe%20erstellen/AppServicePlan.png)



#2
• Erstelle eine zweite Ressourcengruppe "myResourceGroupCli" 
		○ Region: in der entsprechenden paired Region
```sh		
az group create --name myResourceGroupCLI --location westeurope
```			
• Innerhalb dieser Ressourcengruppe erstellst du eine AppService "myWebAppCli"
```sh
az webapp create --resource-group myResourceGroupCLI --name myWebAppCliJanPerp2 --plan myAppServicePlanCli --runtime PYTHON:3.11
```	
• Die Web-App wird auf einem App Service Plan mit dem Namen "myAppServicePlanCli" ausgeführt.

• Die Lösung soll so günstig wie möglich sein. Tipp: SKU beachten
```sh	
az appservice plan create --name myAppServicePlanCli --resource-group myResourceGroupCLI --location westeurope --sku f1 --is-linux
```	
	


#3
• Erstelle eine zweite Ressourcengruppe "myResourceGroupPs" 
		○ Region: in der entsprechenden paired Region
```sh		
New-AzResourceGroup -Name myResourceGroupPS -Location westeurope
```	
• Innerhalb dieser Ressourcengruppe erstellst du eine AppService "myWebAppPs"
```sh
New-AzAppServicePlan -Name myResourceGroupPS -ResourceGroupName myResourceGroupPS -Location westeurope -Tier f1 -WorkerSize 1
```		
• Die Web-App wird auf einem App Service Plan mit dem Namen "myAppServicePlanPs" ausgeführt.

• Die Lösung soll so günstig wie möglich sein. Tipp: SKU beachten
```sh
New-AzWebApp -Name myWebAppPSPerpJan -ResourceGroupName myResourceGroupPS -AppServicePlan myResourceGroupPS -RuntimeStack python:3.11 -OsType <OS_TYPE>
```		


